package com.squareshift.aeroplaneSeating.helper;

import com.squareshift.aeroplaneSeating.model.Seat;

import java.util.PriorityQueue;
import java.util.Queue;

/**
 * Priority implementation class.
 */
public class SeatPicker {

	/**
	 * Priority Queue.
	 */
	private Queue<Seat> queue = new PriorityQueue<>(new SeatComparator());

	/**
	 * Method to add to priority queue.
	 * @param seat seat to add to queue
	 */
	public void add(Seat seat) {
		queue.add(seat);
	}

	/**
	 * Method to fetch a seat from priority queue.
	 * @return seat from queue. {@code null} if the queue is empty
	 */
	public Seat pickSeat() {
		Seat seat = queue.poll();
		if (seat != null) {
			return seat;
		}
		return null;
	}
	
	/**
	 * Reset the priority queue.
	 */
	public void reset() {
		queue = new PriorityQueue<>(new SeatComparator());
	}
}
