package com.squareshift.aeroplaneSeating.helper;

import java.util.Comparator;
import java.util.HashMap;

import com.squareshift.aeroplaneSeating.model.Seat;
import com.squareshift.aeroplaneSeating.model.Seat.SeatType;


/**
 * Comparator class which takes into account the below factors when comparing objects of {@code aeroplaneSeating.Seat}.
 * <p>
 * 1. Type of seat {@literal SeatType.AISLE} {@literal SeatType.WINDOW} {@literal SeatType.MIDDLE}.
 * 2. Row of Seat from front of the plane.
 * 3. Column of seat from left to right.
 */
public class SeatComparator implements Comparator<Seat> {

	/**
	 * Hash map to maintain priority / weightage based on seat type.
	 */
	private static HashMap<SeatType, Integer> map = new HashMap<>();

    /**
     * Populating seat type - weightage hash map.
     */
    static {
        map.put(SeatType.AISLE, 1);
        map.put(SeatType.WINDOW, 2);
        map.put(SeatType.MIDDLE, 3);
    }

    /**
     * Method to compare {@code Seat} by seat type.
     *
     * @param seat1 the first {@code Seat} object to be compared.
     * @param seat2 the second {@code Seat} object to be compared.
     * @return a negative integer, zero, or a positive integer as the first argument is less than, equal to, or greater than the second.
     */
    private static int compareBySeatType(final Seat seat1, final Seat seat2) {
        Integer weight1 = map.get(seat1.getSeatType());
        Integer weight2 = map.get(seat2.getSeatType());
        return Integer.compare(weight1, weight2);
    }

    /**
     * Method to compare by row number of the {@code Seat}.
     *
     * @param seat1 the first {@code Seat} object to be compared.
     * @param seat2 the second {@code Seat} object to be compared.
     * @return a negative integer, zero, or a positive integer as the first argument is less than, equal to, or greater than the second.
     */
    private static int compareByRows(final Seat seat1, final Seat seat2) {
        return Integer.compare(seat1.getRow(), seat2.getRow());
    }

    /**
     * Method to compare by column number of the {@code Seat}.
     *
     * @param seat1 the first {@code Seat} object to be compared.
     * @param seat2 the second {@code Seat} object to be compared.
     * @return a negative integer, zero, or a positive integer as the first argument is less than, equal to, or greater than the second.
     */
    private static int compareByColumns(final Seat seat1, final Seat seat2) {
        return Integer.compare(seat1.getColumn(), seat2.getColumn());
    }

    @Override
    public int compare(final Seat seat1, final Seat seat2) {
        int result = 0;
        result = compareBySeatType(seat1, seat2);
        if (result == 0) {
            result = compareByRows(seat1, seat2);
            if (result == 0) {
                result = compareByColumns(seat1, seat2);
            }
        }
        return result;
    }
}
