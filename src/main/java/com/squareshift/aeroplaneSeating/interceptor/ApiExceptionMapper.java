package com.squareshift.aeroplaneSeating.interceptor;

import com.squareshift.aeroplaneSeating.exception.ApiException;
import lombok.extern.log4j.Log4j2;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
@Log4j2
public class ApiExceptionMapper extends AbstractExceptionMapper implements
        ExceptionMapper<ApiException>
{
    @Override
    public Response toResponse(ApiException e)
    {
        log.error("Intercepted exception: ", e);
        return e.toResponse();
    }
}
