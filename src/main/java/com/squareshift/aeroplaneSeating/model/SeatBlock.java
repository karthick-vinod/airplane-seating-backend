package com.squareshift.aeroplaneSeating.model;
import java.util.ArrayList;
import java.util.LinkedList;

import com.squareshift.aeroplaneSeating.model.Seat.SeatType;
import lombok.Data;


/**
 * Class representing a block of seats in a row.
 *
 * TODO: How to handle a seat block with just one column? WINDOW or AISLE?
 */
@Data
public class SeatBlock {

	/**
	 * List to hold Seats of a block.
	 */
	private ArrayList<Seat> seats = new ArrayList<>();
	
	/**
	 * Type of the block.
	 */
	private BlockType blockType;
	
	/**
	 * SeatVacancy object maintaining the vacancy status of the block. 
	 */
	private SeatBlockVacancy seatVacancy = null;

	/**
	 * Enum to represent varios block types.
	 * @author karthvin
	 *
	 */
	public enum BlockType {
		
		/**
		 * Block located in the left corner of the row.
		 * Structure: WINDOW | MIDDLE | ... | AISLE
		 */
		LEFT_CORNER,
		
		/**
		 * Block located in the middle of the row.
		 * Structure: AISLE | MIDDLE | ... | AISLE
		 */
		MIDDLE,
		
		/**
		 * Block located in the right corner of the row.
		 * Structure: AISLE | MIDDLE | ... | WINDOW
		 */
		RIGHT_CORNER
	}

	/**
	 * Method to add a seat to this seat block.
	 * @param seat seat to be added
	 */
	public final void addSeat(final Seat seat) {
		this.getSeats().add(seat);
	}

	/**
	 * Constructor which takes number of seats in a seat block, row number and
	 * column number of the first seat in the seat block as input.
	 * @param seatsInBlock Number of seats in the block
	 * @param blockType Type of block
	 * @param rowNumber Row number of the first seat in the block
	 * @param columnNumber Column number of the first seat in the block
	 */
	public SeatBlock(final int seatsInBlock, final BlockType blockType, final int rowNumber, final int columnNumber) {
		super();
		this.blockType = blockType;
		createBlock(seatsInBlock, rowNumber, columnNumber);
	}

	/**
	 * Method to create a block given number of seats in a seat block, row number and
	 * column number of the first seat in the seat block
	 * @param seatsInBlock Number of seats in the block
	 * @param rowNumber Row number of the first seat in the block
	 * @param columnNumber Column number of the first seat in the block
	 */
	private void createBlock(final int seatsInBlock, final int rowNumber, int columnNumber) {
		int totalAisleSeats = 0, totalWindowSeats = 0, totalMiddleSeats = 0;
		for (int j = 1; j <= seatsInBlock; j++) {
			SeatType seatType;

			if (this.getBlockType() == BlockType.LEFT_CORNER) {
				if (j == seatsInBlock) {
					seatType = SeatType.AISLE;
					totalAisleSeats++;
				} else if (j == 1) {
					seatType = SeatType.WINDOW;
					totalWindowSeats++;
				} else {
					seatType = SeatType.MIDDLE;
					totalMiddleSeats++;
				}
			} else if (this.getBlockType() == BlockType.RIGHT_CORNER) {
				
				if (j == 1) {
					seatType = SeatType.AISLE;
					totalAisleSeats++;
				} else if (j == seatsInBlock) {
					seatType = SeatType.WINDOW;
					totalWindowSeats++;
				} else {
					seatType = SeatType.MIDDLE;
					totalMiddleSeats++;
				}
			} else {
				if (j == 1 || j == seatsInBlock) {
					seatType = SeatType.AISLE;
					totalAisleSeats++;
				} else {
					seatType = SeatType.MIDDLE;
					totalMiddleSeats++;
				}
			}
			Seat seat = new Seat(seatType, rowNumber, columnNumber);
			this.addSeat(seat);
			columnNumber++;
		}
		this.seatVacancy = new SeatBlockVacancy(totalAisleSeats, totalWindowSeats, totalMiddleSeats);
	}

	/**
	 * Method to get first seat of the seat block.
	 * @return First seat of the seat block
	 */
	private Seat getFirstSeat() {
		ArrayList<Seat> seatsArray = this.getSeats();
		return seatsArray.get(0);
	}

	/**
	 * Method to get last seat of the seat block.
	 * @return Last seat of the seat block
	 */
	private Seat getLastSeat() {
		ArrayList<Seat> seatsArray = this.getSeats();
		return seatsArray.get(seatsArray.size() - 1);
	}

	/**
	 * Method to get all aisle seats in the seat block.
	 * @return List of aisle seats in the seat block
	 */
	private LinkedList<Seat> getAisleSeats() {
		LinkedList<Seat> aisleSeats = new LinkedList<>();
		if (this.getBlockType() == BlockType.LEFT_CORNER) {
			aisleSeats.add(this.getLastSeat());
		} else if (this.getBlockType() == BlockType.RIGHT_CORNER) {
			aisleSeats.add(this.getFirstSeat());
		} else {
			aisleSeats.add(this.getFirstSeat());
			aisleSeats.add(this.getLastSeat());
		}
		return aisleSeats;
	}

	/**
	 * Method to get all window seats in the seat block.
	 * @return List of window seats in the seat block
	 */
	private LinkedList<Seat> getWindowSeats() {
		LinkedList<Seat> windowSeats = new LinkedList<>();
		if (this.getBlockType() == BlockType.LEFT_CORNER) {
			windowSeats.add(this.getFirstSeat());
		} else if (this.getBlockType() == BlockType.RIGHT_CORNER) {
			windowSeats.add(this.getLastSeat());
		} else {
			return null;
		}
		return windowSeats;
	}

	/**
	 * Method to get all middle seats in the seat block.
	 * @return List of middle seats in the seat block
	 */
	private LinkedList<Seat> getMiddleSeats() {
		LinkedList<Seat> middleSeats = new LinkedList<>(getSeats());
		middleSeats.removeFirst();
		middleSeats.removeLast();
		return middleSeats;

	}

	/**
	 * Method to find if the seat block has a vacan seat of given seat type. 
	 * @param seatType seat type
	 * @return {@code true} if the block has a vacant seat of type {@code seatType}, {@code false} otherwise
	 */
	public boolean hasVacancy(final SeatType seatType) {
		return getSeatVacancy().canAssign(seatType);
	}

	/**
	 * Method to assign a vacant seat to the passenger. 
	 * @param seatType Type of seat preference
	 * @param passenger Passenger to whom a seat must be allocated to.
	 */
	public void assignVacantSeat(final SeatType seatType, final int passenger) {
		if (getSeatVacancy().validateAndAssign(seatType)) {
			getVacantSeat(seatType).assignPassenger(passenger);
		}
	}

	/**
	 * Method to find a vacant seat of given seat type.
	 * @param seatType seat type
	 * @return Vacant seat of type {@code seatType}  
	 */
	public Seat getVacantSeat(final SeatType seatType) {
		if (!this.hasVacancy(seatType)) {
			return  null;
		}
		LinkedList<Seat> seats;
		if (seatType == SeatType.AISLE) {
			seats = this.getAisleSeats();
		} else if (seatType == SeatType.WINDOW) {
			seats = this.getWindowSeats();
		} else {
			seats = this.getMiddleSeats();
		}
		return seats.stream().filter(Seat::isVacant).findFirst().orElse(null);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		int totalSeats = getSeatVacancy().totalSeats;
		if (totalSeats == 0) {
			totalSeats = 1;
		}
		for (Seat seat : seats) {
			builder.append(seat);
		}
		return builder.toString();
	}
}
