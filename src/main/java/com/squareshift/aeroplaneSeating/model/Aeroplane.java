package com.squareshift.aeroplaneSeating.model;

import com.squareshift.aeroplaneSeating.helper.SeatPicker;
import com.squareshift.aeroplaneSeating.util.ExceptionUtil;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.ws.rs.core.Response;
import java.util.ArrayList;

/**
 * Class representing an aeroplane.
 */
@Data
public class Aeroplane {

	private final String id;
	
	/**
	 * Matrix representing arrangement of seats in an aeroplane.
	 */
	private Integer[][] seatStructure;
	
	/**
	 * Variable to hold the transpose of seatStructure matrix.
	 */
	@Getter(AccessLevel.NONE)
	@Setter(AccessLevel.NONE)
	private Integer[][] transposedSeatStructure;
	
	/**
	 * List of {@code SeatRows}.
	 */
	private ArrayList<SeatRow> seatRows = new ArrayList<>();

	private int seatsAllocatedSoFar = 0;


	@Getter(AccessLevel.NONE)
	@Setter(AccessLevel.NONE)
	private SeatPicker seatPicker = new SeatPicker();

	/**
	 * Constructor which takes seat structure matrix as input and initializes seat allocation logic.
	 * @param seatStructure Structure of seats as a M X 2 matrix with
	 * each row is a tuple of {@code (number of seats in a row, number of rows in a block)}
	 */
	public Aeroplane(String id, Integer[][] seatStructure) {
		super();
		this.id = id;
		this.seatStructure = seatStructure;
		this.transposedSeatStructure = this.transpose(this.seatStructure);
		
		/*
		 * Array to hold number of seats in a row per block.
		 */
		Integer[] seatsPerRow = transposedSeatStructure[0].clone();
		
		/*
		 * Array to hold number of rows in each block.
		 */
		Integer[] rowsPerBlock = transposedSeatStructure[1].clone();
		Integer loopIterations = 0;
		
		if (rowsPerBlock.length > 0) loopIterations = max(rowsPerBlock);
		
		if (loopIterations == null) loopIterations = 0;
		
		/*
		 * This loop creates one SeatRow per iteration based on seats per block for current row. 
		 */
		for (int i = 0; i < loopIterations; i++) {
			seatRows.add(new SeatRow(seatsPerRow, i));
			nonNegativeDecrement(rowsPerBlock);
			handleBlockCounts(rowsPerBlock, seatsPerRow);
		}

		/*
		  Add all seats of airplane into seatpicker (Priority Queue)
		 */
		initializeSeatPicker();
	}

	/**
	 * Add all the seats of airplane into priority queue.
	 */
	private void initializeSeatPicker() {
		seatRows.forEach(row -> row.getSeatBlocks().forEach(block -> block.getSeats().forEach(seat -> {
			seat.resetPassenger();
			seatPicker.add(seat);
		})));
	}

	/**
	 * If there are zero rows in a block, mark number of seats to zero as well.
	 * @param rowsPerBlock rows per block
	 * @param seatsPerRow seats per row
	 */
	private void handleBlockCounts(Integer[] rowsPerBlock, Integer[] seatsPerRow) {
		for (int i = 0; i < rowsPerBlock.length; i++) {
			if (rowsPerBlock[i] == 0) {
				seatsPerRow[i] = 0;
			}
		}
	}

	/**
	 * Method to transpose a matrix.
	 * @param originalMatrix Matrix to transpose
	 * @return transposed matrix
	 */
	private Integer[][] transpose(Integer[][] originalMatrix) {
		Integer[][] transposedMatrix = new Integer[2][originalMatrix.length];  

		//Code to transpose a matrix
		for(int i = 0; i < originalMatrix.length; i++){    
			for(int j = 0; j < originalMatrix[i].length; j++){    
				transposedMatrix[j][i] = originalMatrix[i][j];
			}    
		} 
		return transposedMatrix;
	}

	/**
	 * Method to decrement all the positive elements in an array by one.
	 * @param array Array to decrement.
	 */
	private void nonNegativeDecrement(Integer[] array) {
		for (int i = 0; i < array.length; i++) {
			if (array[i] > 0) {
				array[i] = --array[i];
			}
		}
	}

	/**
	 * Method to find the maximum element in an array.
	 * @param array Array to find the max element from.
	 * @return Max value
	 */
	private Integer max(Integer[] array) {
		Integer max = array[0];
		for (int i = 1; i < array.length; i++) {
			if (array[i] > max) {
				max = array[i];
			}
		}
		return max;
	}

	/**
	 * Method to allocate a seat to a passenger in airplane.
	 * TODO: For now, {@code passenger} is just an integer. This method may need to accept a passenger object to associate with the seat.
	 * @return status. True if the seat is successfully allocated, false otherwise.
	 */
	public boolean allocateSeat() {
		int passenger = this.seatsAllocatedSoFar + 1;
		Seat seat = seatPicker.pickSeat();
		if (seat == null) {
			System.err.println("Cannot allocate seat to Passenger "+ (this.seatsAllocatedSoFar + 1) + ". Plane Full!!");
			ExceptionUtil.throwError(Response.Status.PRECONDITION_FAILED, "Cannot allocate seat to Passenger "+ (this.seatsAllocatedSoFar + 1) + ". Plane Full!!");
			return false;
		}
		seat.assignPassenger(passenger);
		this.seatsAllocatedSoFar++;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Aeroplane : {\n");
		for (SeatRow seatRow : this.seatRows) {
			builder.append("    || ");
			ArrayList<SeatBlock> seatBlocks = seatRow.getSeatBlocks();
			for (int i = 0; i < seatBlocks.size(); i++) {
				int maxSeats = transposedSeatStructure[0][i];
				if (maxSeats == 0) {
					maxSeats = 1;
				}
				int pad = maxSeats * 15;
				builder.append(String.format("%1$-"+pad+"s", seatBlocks.get(i).toString()));
				builder.append(" | \\/\\/\\/\\/ | ");
			}
			builder.replace(builder.length() - 14, builder.length(), "||");
			builder.append("\n");
		}
		builder.append("}\n");
		return builder.toString();
	}
	
	/**
	 * Reset airplane.
	 */
	public void reset() {
		seatPicker.reset();
		initializeSeatPicker();
	}
}
