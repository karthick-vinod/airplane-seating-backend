package com.squareshift.aeroplaneSeating.model;

import com.squareshift.aeroplaneSeating.model.Seat.SeatType;

public class SeatBlockVacancy {
	public int totalSeats;
	private int totalVacantAisleSeats;
	private int totalVacantWindowSeats;
	private int totalVacantMiddleSeats;

	/**
	 * @param totalAisleSeats
	 * @param totalWindowSeats
	 * @param totalMiddleSeats
	 */
	public SeatBlockVacancy(final int totalAisleSeats, final int totalWindowSeats, final int totalMiddleSeats) {
		super();
		totalSeats = totalAisleSeats + totalWindowSeats + totalMiddleSeats;
	}

	private void assign(SeatType seatType) {
		if (seatType == SeatType.AISLE) {
			this.totalVacantAisleSeats--;
		} else if (seatType == SeatType.WINDOW) {
			this.totalVacantWindowSeats--;
		} else {
			this.totalVacantMiddleSeats--;
		}
	}

	public boolean validateAndAssign(Seat.SeatType seatType) {
		if (this.canAssign(seatType)) {
			this.assign(seatType);
			return true;
		}
		return false;
	}

	public boolean canAssign(Seat.SeatType seatType) {
		if (
				(seatType == SeatType.AISLE && this.totalVacantAisleSeats > 0)
				|| (seatType == SeatType.WINDOW && this.totalVacantWindowSeats > 0)
				|| (seatType == SeatType.MIDDLE && this.totalVacantMiddleSeats > 0)
				) {
			return true;
		}
		return false;
	}
}
