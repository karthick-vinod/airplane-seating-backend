package com.squareshift.aeroplaneSeating.model;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

/**
 * Class to represent a single seat of an aeroplane.
 */
@Data
@RequiredArgsConstructor
public class Seat {

	/**
	 * Types of Seats.
	 */
	public enum SeatType {
		AISLE, MIDDLE, WINDOW;
	}

	/**
	 * Variable to hold Seat Type of the seat.
	 */
	@NonNull private SeatType seatType;

	/**
	 * Row number of the seat.
	 */
	@NonNull private int row;

	/**
	 * Column number of the seat.
	 */
	@NonNull private int column;
	
	/**
	 * Passenger occupying the seat.
	 */
	private Integer passenger = null;

	/**
	 * Method to assign a passenger to {@code this} seat.
	 * @param passenger the passenger to assign
	 */
	public final void assignPassenger(final int passenger) {
		this.passenger = passenger;
	}

	/**
	 * Method to determine if the seat is vacant.
	 * @return true if seat is vacant, false otherwise
	 */
	public final boolean isVacant() {
		return this.getPassenger() == null;
	}

	public final void resetPassenger() {this.setPassenger(null);}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format("(%1$-4s : %2$4s)", passenger, seatType.toString());
	}
}
