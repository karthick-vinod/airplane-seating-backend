package com.squareshift.aeroplaneSeating.model;

import com.squareshift.aeroplaneSeating.model.SeatBlock.BlockType;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;


/**
 * Class representing a single row of seats across all seatBlocks in an airplane.
 */
@Data
public class SeatRow {

    /**
     * Variable to hold pattern of seats in a row.
     * Each integer represents number of seats per block in a row.
     */
    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    private Integer[] seatRowStructure = null;

    /**
     * List of SeatBlock objects.
     * seatBlocks.get(X) holds a seat block object with seatRowStructure[X] seats in order.
     */
    private ArrayList<SeatBlock> seatBlocks = new ArrayList<SeatBlock>();

    /**
     * Constructor which takes seat structure matrix and row number as input.
     *
     * @param seatStructure Structure of seats as an array with
     *                      each element representing number of seats per block in the current row.
     * @param rowNumber     row number
     */
    public SeatRow(final Integer[] seatStructure, final int rowNumber) {
        super();
        this.seatRowStructure = seatStructure;
        int columnNumber = 0;

        for (int i = 0; i < seatStructure.length; i++) {
            int seatsInBlock = seatStructure[i];
            if (i == 0) {
                createLeftCornerBlock(seatsInBlock, rowNumber, columnNumber);
            } else if (i == seatStructure.length - 1) {
                createRightCornerBlock(seatsInBlock, rowNumber, columnNumber);
            } else {
                createMiddleBlock(seatsInBlock, rowNumber, columnNumber);
            }
            columnNumber = columnNumber + seatsInBlock;
        }
    }

//    /**
//     * @return the seatBlocks list
//     */
//    private final ArrayList<SeatBlock> getSeatBlocks() {
//        return seatBlocks;
//    }

    /**
     * Method to add a seat block to seat blocks list.
     *
     * @param seatBlock seatBlock to be added
     */
    private final void addSeatBlock(final SeatBlock seatBlock) {
        this.getSeatBlocks().add(seatBlock);
    }

    /**
     * Method to create a left corner block. A block of the format WINDOW | MIDDLE | ... | AISLE.
     *
     * @param seatsInBlock Number of seats in block
     * @param rowNumber    Row number
     * @param columnNumber Column number
     */
    private void createLeftCornerBlock(final int seatsInBlock, final int rowNumber, final int columnNumber) {
        this.addSeatBlock(new SeatBlock(seatsInBlock, SeatBlock.BlockType.LEFT_CORNER, rowNumber, columnNumber));
    }

    /**
     * Method to create a right corner block. A block of the format AISLE | MIDDLE | ... | WINDOW.
     *
     * @param seatsInBlock Number of seats in block
     * @param rowNumber    Row number
     * @param columnNumber Column number
     */
    private void createRightCornerBlock(final int seatsInBlock, final int rowNumber, final int columnNumber) {
        this.addSeatBlock(new SeatBlock(seatsInBlock, SeatBlock.BlockType.RIGHT_CORNER, rowNumber, columnNumber));
    }

    /**
     * Method to create a middle block. A block of the format AISLE | MIDDLE | ... | AISLE.
     *
     * @param seatsInBlock Number of seats in block
     * @param rowNumber    Row number
     * @param columnNumber Column number
     */
    private void createMiddleBlock(final int seatsInBlock, final int rowNumber, final int columnNumber) {
        this.addSeatBlock(new SeatBlock(seatsInBlock, BlockType.MIDDLE, rowNumber, columnNumber));
    }
}
