package com.squareshift.aeroplaneSeating.controller;

import com.squareshift.aeroplaneSeating.model.Aeroplane;
import com.squareshift.aeroplaneSeating.AeroplaneFactory;
import com.squareshift.aeroplaneSeating.service.SeatAllocationService;
import com.squareshift.aeroplaneSeating.util.ExceptionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Collection;

@Path("/aeroplanes")
@Produces("application/json")
@CrossOrigin
@RestController
public class AeroplaneController {

    @Autowired
    private SeatAllocationService seatAllocationService;

    @GET
    public Collection<Aeroplane> listAeroplanes() {
        return AeroplaneFactory.getAllAeroplanes();
    }

    @POST
    @Path("/{aeroplaneId}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Aeroplane createAeroplane(@PathParam("aeroplaneId") String aeroplaneId, @RequestBody(required = true) Integer[][] seatStructure){
        return AeroplaneFactory.getAeroplane(aeroplaneId, seatStructure);
    }

    @GET
    @Path("/{aeroplaneId}")
    public Aeroplane getAeroplane(@PathParam("aeroplaneId") String aeroplaneId) {
        Aeroplane aeroplane = AeroplaneFactory.getAeroplane(aeroplaneId);
        if(aeroplane == null) {
            ExceptionUtil.throwError(Response.Status.NOT_FOUND, "Cannot find the requested aeroplane");
        }
        return aeroplane;
    }

    @POST
    @Path("/{aeroplaneId}/allocate_seat")
    @Consumes(MediaType.APPLICATION_JSON)
    public Aeroplane allocateSeat(@PathParam("aeroplaneId") String aeroplaneId){
        return seatAllocationService.allocateSeat(aeroplaneId);
    }

    @POST
    @Path("/{aeroplaneId}/reset")
    @Consumes(MediaType.APPLICATION_JSON)
    public void reset(@PathParam("aeroplaneId") String aeroplaneId){
        Aeroplane aeroplane = AeroplaneFactory.getAeroplane(aeroplaneId);
        if(aeroplane == null) {
            ExceptionUtil.throwError(Response.Status.NOT_FOUND, "Cannot find the requested aeroplane");
        }
        aeroplane.reset();
    }
}
