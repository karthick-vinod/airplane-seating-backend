package com.squareshift.aeroplaneSeating.service;

import com.squareshift.aeroplaneSeating.model.Aeroplane;
import com.squareshift.aeroplaneSeating.AeroplaneFactory;
import org.springframework.stereotype.Service;

@Service
public class SeatAllocationService {
    public Aeroplane allocateSeat(String id) {
        Aeroplane aeroplane = AeroplaneFactory.getAeroplane(id);
        aeroplane.allocateSeat();
        return AeroplaneFactory.getAeroplane(id);
    }
}
