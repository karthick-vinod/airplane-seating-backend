package com.squareshift.aeroplaneSeating;

import com.squareshift.aeroplaneSeating.model.Aeroplane;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * This is just a helper class. Added as a hack to mimic functionality of database
 */
public class AeroplaneFactory {
    private static Map<String, Aeroplane> database = new HashMap<>();

    public static Aeroplane getAeroplane(String id) {
        return database.get(id);
    }

    public static Aeroplane getAeroplane(String id, Integer[][] input) {
        if(database.containsKey(id)) {
            return database.get(id);
        }
        Aeroplane aeroplane = new Aeroplane(id, input);
        AeroplaneFactory.putAeroplane(aeroplane);
        return aeroplane;
    }

    public static void putAeroplane(Aeroplane aeroplane) {
        database.put(aeroplane.getId(), aeroplane);
    }

    public static Collection<Aeroplane> getAllAeroplanes() {
        return database.values();
    }
}
