package com.squareshift.aeroplaneSeating.util;

import com.squareshift.aeroplaneSeating.exception.ApiException;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.Response;

@Service
@Log4j2
public class ExceptionUtil {
    public static void throwError(Response.Status status, String msg) {
        throw new ApiException(status, msg);
    }
}
