package com.squareshift.aeroplaneSeating;

import com.squareshift.aeroplaneSeating.model.Aeroplane;

/**
 * Class to instantiate, provide input and print output to Aeroplane class.
 *
 * @author karthick-vinod
 *
 */
public class AeroplaneInitializer {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		/**
		 * Test Cases
		 */

		/**
		 * Test case 1 - Case from problem statement.
		 */
		Integer[][] sample1 = {{3,2}, {4,3}, {2,3}, {3,4}};
		test(sample1, 30);

		/**
		 * Test case 2 - Normal scenario.
		 */
		Integer[][] sample2 = {{1,2}, {3,4}};
		test(sample2, 10);

		/**
		 * Test case 3 - Normal scenario.
		 */
		Integer[][] sample3 = {{1,2}, {3,4}, {1,2}};
		test(sample3, 15);

		/**
		 * Test case 4 - Passenger count is greater than seat count in the plane.
		 */
		Integer[][] sample4 = {{1,5}, {3,4}, {5,2}};
		test(sample4, 1100);

		/**
		 * Test case 5 - Single block, single column plane seat structure.
		 * If a seat has window on one side and aisle on the other, treating it as an aisle seat
		 */
		Integer[][] sample5 = {{1,5}};
		test(sample5, 2);

		/**
		 * Test case 6 - Two blocks, single column each in plane seat structure.
		 * If a seat has window on one side and aisle on the other, treating it as an aisle seat
		 */
		Integer[][] sample6 = {{1,5}, {1,2}};
		test(sample6, 2);

		/**
		 * Test case 7 - Test {0,0} in seat structure.
		 */
		Integer[][] sample7 = {{1,5}, {0,0}, {0,0}, {1,5}};
		test(sample7, 9);

		/**
		 * Test case 8 -  Test {0,X} in seat structure.
		 */
		Integer[][] sample8 = {{1,5}, {0,8}, {0,1}, {1,5}};
		test(sample8, 11);

		/**
		 * Test case 9 -  Test {0,X} in seat structure.
		 */
		Integer[][] sample9 = {{0,0}};
		test(sample9, 11);

		/**
		 * Test case 10 -  Test 0 X 0 matrix input in seat structure.
		 */
		Integer[][] sample10 = {{}};
		test(sample10, 11);
	}

	/**
	 * Method to execute each of the cases mentioned above.
	 * @param sample Seating structure matrix to test with
	 * @param passengerCount Number of passengers to test with 
	 */
	public static void test(Integer[][] sample, int passengerCount) {
		Aeroplane aeroplane = new Aeroplane("Aeroplane", sample);
		try {

			for (int i = 0; i < passengerCount; i++) {
				if (!aeroplane.allocateSeat()) {
					break;
				}
			}

		} catch (Exception e) {

		} finally {
			System.out.println(aeroplane.toString());
		}
		System.out.println("=================");
	}
}
