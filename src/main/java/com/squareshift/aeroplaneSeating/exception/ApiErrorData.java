package com.squareshift.aeroplaneSeating.exception;

import lombok.Data;

@Data
public class ApiErrorData {
    String description;

    public static ApiErrorData forError(String description) {
        ApiErrorData error = new ApiErrorData();
        error.setDescription(description);
        return error;
    }
}
