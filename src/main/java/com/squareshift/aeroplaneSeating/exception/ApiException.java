package com.squareshift.aeroplaneSeating.exception;

import lombok.Data;

import javax.ws.rs.core.Response;

@Data
public class ApiException extends RuntimeException {
    String description;
    Response.Status code;
    Response response;

    public ApiException(Response.Status code, String description) {
        this.code = code;
        this.description = description;
    }

    public Response toResponse() {
        return Response.status(code).entity(ApiErrorData.forError(description)).build();
    }
}
