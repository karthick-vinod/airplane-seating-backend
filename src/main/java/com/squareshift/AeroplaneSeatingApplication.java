package com.squareshift;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AeroplaneSeatingApplication {

    public static void main(String[] args) {
        SpringApplication.run(AeroplaneSeatingApplication.class, args);
    }
}

