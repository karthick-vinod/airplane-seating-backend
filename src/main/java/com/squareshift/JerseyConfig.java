package com.squareshift;

import com.squareshift.aeroplaneSeating.controller.AeroplaneController;
import com.squareshift.aeroplaneSeating.interceptor.ApiExceptionMapper;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JerseyConfig extends ResourceConfig {

    public JerseyConfig() {
        register(ApiExceptionMapper.class);
        register(AeroplaneController.class);
    }
}
