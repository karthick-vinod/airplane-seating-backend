package com.squareshift;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.text.SimpleDateFormat;
import java.util.function.Consumer;

public class ObjectMapperFactory {
    private static void configureDefaultValues(ObjectMapper mapper) {
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX"));
    }

    public static ObjectMapper createObjectMapper(Consumer<ObjectMapper> consumer) {
        ObjectMapper mapper = createDefaultObjectMapper();
        consumer.accept(mapper);
        return mapper;
    }

    public static ObjectMapper createDefaultObjectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        configureDefaultValues(mapper);
        return mapper;
    }
}
