package com.squareshift;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GeneralConfig {
    @Bean
    public ObjectMapper mapper() {
        return ObjectMapperFactory.createDefaultObjectMapper();
    }

}
